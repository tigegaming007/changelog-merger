import json
import os
import sys
import shutil

def openallebestanden(map):
    return next(os.walk(map))[2]
    # input source dir into def var 'map'


# out = bestandsnamen array.

def extractvaluefromfile(filepad):
    with open(filepad) as f:
        inhoud = f.read()
        return json.loads(inhoud)

    # array -> dict
    # return [0]["description"]
    # file[i]["date"]


#def dataopvolgorde(output_extraction):

def schrijfnaarbestand(lijst, version):
    if os.path.exists("output.md"):
        with open("temp.md", "w+") as f:
            f.write("# Changelogs\n")
            f.write("# " + version + "\n\n")
            for l in lijst:
                f.write(l)
            with open("output.md") as old:
                next(old)
                for line in old:
                    f.write(line)
        os.remove('output.md')
        os.rename('temp.md', 'output.md')

    else:
        with open("output.md", "a+") as f:
            f.write("# Changelogs\n")
            f.write("# " + version + "\n\n")
            for l in lijst:
                f.write(l)



    print("klaar!")

if __name__ == '__main__':
    print(sys.argv)
    version = "Version " + sys.argv[1]
    files = openallebestanden("./unrel")
    print(files)
    final_list = list()
    for f in files:
        pad = "./unrel/" + f
        data = extractvaluefromfile(pad)
        date = data["date"]
        description = data["description"]
        final = "- " + date + " - " + description + "\n"
        print(final)
        final_list.append(final)
    final_list.sort(reverse=True)
    print(final_list)
    schrijfnaarbestand(final_list, version)
    if not os.path.exists(version):
        os.makedirs(version)
    for f in files:
        shutil.move("./unrel/" + f, version)